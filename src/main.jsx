import './index.css'
import React from 'react'
import ReactDOM from 'react-dom/client'
import MainComponent from './components/'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <MainComponent />
  </React.StrictMode>
)
