import { RiTodoLine } from "react-icons/ri";
import React, { useRef, useState } from "react";

function MainComponent() {
  const toDoNameRef = useRef();
  const [toDos, setToDos] = useState([]);
  const [editToDo, setEditToDo] = useState(false);
  const [editToDoID, setEditToDoID] = useState("");
  const [taskCompleted, setTaskCompleted] = useState(false)
  
  const handleToDoSubmit = (_) => {
    let name = toDoNameRef.current.value;
    console.log(name);
    if (name === "") {
      return;
    } else if (editToDo) {
      const copyOfToDos = [...toDos];
      const findTodo = copyOfToDos.find((value) => value.id === editToDoID);
      findTodo.title = name;
    } else {
      setToDos((prevValue) => {
        return [
          ...prevValue,
          {
            title: name,
            completed: false,
            id: new Date().getTime().toString()
          }
        ];
      });
    }
    setEditToDo(false);
    toDoNameRef.current.value = null;
  };

  const toggleCompletedToDos = (event, toDoID) => {
    const copyOfToDos = [...toDos];
    const findTodo = copyOfToDos.find((value) => value.id === toDoID);
    findTodo.completed = !findTodo.completed;
    setTaskCompleted(prevValue => !prevValue)
  };

  const handleToDoEdit = (event, toDoID) => {
    event.preventDefault();
    setEditToDo(true);
    setEditToDoID(toDoID);
    const copyOfToDos = [...toDos];
    const findTodo = copyOfToDos.find((value) => value.id === toDoID);
    toDoNameRef.current.value = findTodo.title;
  };

  return (
    <div className="bg-slate-50 h-screen w-screen">
      <div className="flex justify-center pt-14">
        <RiTodoLine size={160} />
      </div>
      <section className="py-8">
        <div className="text-center text-4xl">RIOS - Todo List Application</div>
      </section>
      {/* Input field */}
      <section className="flex flex-col">
        <input
          type="text"
          ref={toDoNameRef}
          className="border w-2/4 h-10 mx-auto" 
          style={editToDo ? {border: '2px red solid'} : {border: '1px solid #ccc'}}
        />
        <button
          onClick={handleToDoSubmit}
          className="mt-4 mb-16 text-lg border w-[180px] h-[50px] mx-auto border-black border-solid"
        >
          Submit
        </button>
      </section>
      {/* List of ToDos */}
      <section className="px-8 mx-auto md:w-[900px]">
        <ul>
          {toDos.map((value) => (
            <div
              key={value.id}
              className="flex text-center justify-between w-full md:w-1200px"
            >
              <li
                className="text-2xl mb-8"
                style={
                  value.completed
                    ? { textDecoration: "line-through" }
                    : { textDecoration: "none" }
                }
              >
                {value.title}
              </li>
              <div>
                <button
                  className="border mr-4 text-md border-black border-solid px-8"
                  onClick={(e) => handleToDoEdit(e, value.id)}
                >
                  Edit
                </button>
                <button
                  onClick={(e) => toggleCompletedToDos(e, value.id)}
                  className="border text-md border-black border-solid px-4"
                >
                  Completed
                </button>
              </div>
            </div>
          ))}
        </ul>
      </section>
    </div>
  );
}

export default MainComponent;
